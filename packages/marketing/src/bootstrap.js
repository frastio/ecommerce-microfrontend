import React from "react";
import ReactDOM from "react-dom";

const mount = (el) => {
  ReactDOM.render(<h1>hala</h1>, el);
};
if (process.env.NODE_ENV === "development") {
  const devRoot = document.querySelector("#_marketing-dev-root");

  if (devRoot) mount(devRoot);
}

// Generic mount function to be imported on the container
// A container shouldn't assume that a child is using a particular framework
export { mount };
