const webpack = require("webpack");
const { merge } = require("webpack-merge");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const commonConfig = require("./webpack.common");

const deps = require("../package.json").dependencies;

/** @type {import('webpack').Configuration} */
const prodConfig = {
  mode: "development",
  output: {
    filename: "[name].[contenthash].js",
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "marketing",
      filename: "remoteEntry.js",
      exposes: {
        "./MarketingApp": "./src/bootstrap",
      },
      shared: { ...deps },
    }),

    new webpack.ProvidePlugin({
      process: "process/browser",
    }),
  ],

  resolve: {
    alias: {
      process: "process/browser",
    },
  },
};

module.exports = merge(commonConfig, prodConfig);
