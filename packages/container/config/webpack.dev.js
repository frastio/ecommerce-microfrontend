const webpack = require("webpack");
const { merge } = require("webpack-merge");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const commonConfig = require("./webpack.common");

const deps = require("../package.json").dependencies;

/** @type {import('webpack').Configuration} */
const devConfig = {
  mode: "development",
  devServer: {
    port: 8080,
    historyApiFallback: {
      index: "index.html",
    },
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "container",
      remotes: {
        // [...]@host the prefix should be match with the MFP remote name
        marketing: "marketing@http://localhost:8081/remoteEntry.js",
      },
      shared: { ...deps },
    }),

    // webpack 5 specific setup to make process object available on the browser
    new webpack.ProvidePlugin({
      process: "process/browser",
    }),
  ],

  resolve: {
    alias: {
      process: "process/browser",
    },
  },
};

module.exports = merge(commonConfig, devConfig);
