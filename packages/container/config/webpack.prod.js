const webpack = require("webpack");
const { merge } = require("webpack-merge");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const commonConfig = require("./webpack.common");

const deps = require("../package.json").dependencies;

const host = process.env.HOST;

/** @type {import('webpack').Configuration} */
const prodConfig = {
  mode: "production",
  output: {
    filename: "[name].[contenthash].js",
    publicPath: "/container/latest/",
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "container",
      remotes: {
        // [...]@host the prefix should be match with the MFP remote name
        marketing: `marketing@${host}/marketing/remoteEntry.js`,
      },
      shared: { ...deps },
    }),

    // webpack 5 specific setup to make process object available on the browser
    new webpack.ProvidePlugin({
      process: "process/browser",
    }),
  ],

  resolve: {
    alias: {
      process: "process/browser",
    },
  },
};

module.exports = merge(commonConfig, prodConfig);
